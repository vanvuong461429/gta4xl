#
# Copyright (C) 2024 The Android Open Source Project
# Copyright (C) 2024 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

add_lunch_combo omni_gta4xl-user
add_lunch_combo omni_gta4xl-userdebug
add_lunch_combo omni_gta4xl-eng
