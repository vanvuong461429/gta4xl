#
# Copyright (C) 2024 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_gta4xl.mk

COMMON_LUNCH_CHOICES := \
    lineage_gta4xl-user \
    lineage_gta4xl-userdebug \
    lineage_gta4xl-eng
